import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkJsonSchema,
  } from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import {
  schema_allUsers,
  schema_login,
  schema_user,
  schema_post,
} from "./data/schemas_testData.json";
import { PostsController } from "../lib/controllers/posts.controller";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const posts = new PostsController();
const chance = new Chance();

let registeredUserEmail: string;
let registeredUserPassword: string;
let registeredUserAvatar: string;
let registeredUserName: string;
let registeredUserId: string;
let accessUserToken: string;
let loginResponse: any;
let postId: number;
let body: string;

describe("User flow", () => {
  before("Register and login", async () => {
    registeredUserEmail = chance.email();
    registeredUserPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredUserEmail,
      registeredUserPassword,
      registeredUserName
    );
    registeredUserId = registrationResponse.body.user.id;
    registeredUserAvatar = registrationResponse.body.user.avatar;

    loginResponse = await auth.login(registeredUserEmail, registeredUserPassword);
    accessUserToken = loginResponse.body.token.accessToken.token;
  });

  
  it("Get list of users", async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkJsonSchema (response, schema_allUsers)
    expect(response.body.length).to.be.greaterThan(0);
  });

  it("Get list of posts", async () => {
    const response = await posts.getListOfPosts();
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkJsonSchema(response,schema_post)
    expect(response.body).to.be.an("array").that.is.not.empty;
  });

  it("Create of new post", async () => {
    body = chance.string({
      length: 20,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    const newPost = {
      authorId: registeredUserId,
      previewImage: undefined,
      body: body,
    };
    const response = await posts.createNewPost(newPost, accessUserToken);
    postId = response.body.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkJsonSchema(response, schema_post)
    expect(response.body.body).to.equal(newPost.body);
  });

  it("Add like to the post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.addLikeForPost(newPostLike, accessUserToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getListOfPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(addedLike).to.exist;
  });

  it("Delete like from the post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.deleteLikeForPost(newPostLike, accessUserToken);
  
    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
  
    const allPostsResponse = await posts.getListOfPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const deletedLike = createdPost.reactions.find(
      (like) => like.user.id === registeredUserId
    );
  
    expect(deletedLike).to.be.undefined;
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessUserToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
