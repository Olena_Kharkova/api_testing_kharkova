import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkJsonSchema,
  checkResponseTime,
  checkStatusCode,
  validateSchema,
} from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import {
  schema_post,
  schema_comment,
  schema_user,
} from "./data/schemas_testData.json";
import { PostsController } from "../lib/controllers/posts.controller";

const users = new UsersController();
const authorization = new AuthController();
const registration = new RegController();
const posts = new PostsController();
const chance = new Chance();

let registeredUserEmail: string;
let registeredUserPassword: string;
let registeredUserName: string;
let registeredUserId: string;
let accessUserToken: string;
let body: string;
let postId: number;
let commentBody: string;
let loginResponse: any;

describe("Happy Path Tests post", () => {
  before("Register and login", async () => {
    registeredUserEmail = chance.email();
    registeredUserPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await registration.registration(
      registeredUserEmail,
      registeredUserPassword,
      registeredUserName
    );
    registeredUserId = registrationResponse.body.user.id;

    loginResponse = await authorization.login(registeredUserEmail, registeredUserPassword);
    accessUserToken = loginResponse.body.token.accessToken.token;
  });

  it("Create the new post", async () => {
    body = chance.string({
      length: 20,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    const newPost = {
      authorId: registeredUserId,
      previewImage: undefined,
      body: body,
    };
    const response = await posts.createNewPost(newPost, accessUserToken);
    postId = response.body.id;

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkJsonSchema (response, schema_post);
    expect(response.body.body).to.equal(newPost.body);
  });

  it("Check creation of new post", async () => {
    const allPostsResponse = await posts.getListOfPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    expect(createdPost).to.exist;
    expect(createdPost.author.userName).to.equal(registeredUserName);

    checkStatusCode(allPostsResponse, 200);
    checkResponseTime(allPostsResponse, 1000);
    checkJsonSchema (allPostsResponse, schema_post);
  });

  it("Add like to the post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.addLikeForPost(newPostLike, accessUserToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    
    const allPostsResponse = await posts.getListOfPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const addedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );
 
    expect(addedLike).to.exist; 
  });

  it("Delete like from the post", async () => {
    const newPostLike = {
      entityId: postId,
      isLike: true,
      userId: registeredUserId,
    };
    const response = await posts.deleteLikeForPost(newPostLike, accessUserToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);

    const allPostsResponse = await posts.getListOfPosts();
    const allPosts = allPostsResponse.body;
    const createdPost = allPosts.find((post) => post.id === postId);
    const deletedLike = createdPost.reactions.find(
      (like: any) => like.user.id === registeredUserId
    );

    expect(deletedLike).to.be.undefined;
  });

  it("Adding comment to the Post", async () => {
    commentBody = "Hello World!";
    const newComment = {
      authorId: registeredUserId,
      postId: postId,
      body: commentBody,
    };

    const addCommentResponse = await posts.addNewComment(newComment, accessUserToken);

    checkStatusCode(addCommentResponse, 200);
    checkResponseTime(addCommentResponse, 1000);
    checkJsonSchema(addCommentResponse,schema_comment)
    expect(addCommentResponse.body.body).to.be.deep.equal(commentBody);
    
  });

  it("Adding one more comment to the Post", async () => {
    commentBody = "Goodbye!";
    const newComment = {
      authorId: registeredUserId,
      postId: postId,
      body: commentBody,
    };

    const addCommentResponse = await posts.addNewComment(newComment, accessUserToken);

    checkStatusCode(addCommentResponse, 200);
    checkResponseTime(addCommentResponse, 1000);
    checkJsonSchema(addCommentResponse,schema_comment)
    expect(addCommentResponse.body.body).to.be.deep.equal(commentBody);
    
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessUserToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
