import { expect } from "chai";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import { checkResponseTime } from "../../helpers/functionsForChecking.helper";
import Chance from "chance";

const authorization = new AuthController();
const registration = new RegController();
const chance = new Chance();
const INVALID_PASSWORD_ERROR_MESSAGE = "Invalid username or password.";

let registeredUserEmail: string;
let registeredUserPassword: string;
let registeredUserName: string;
let registeredUserId: number;
let registeredUserAvatar: string;


describe("Negative tests", () => {
  let loginResponse: any;

  it(`Invalid email registration`, async () => {
    const invalidEmail = chance.string({
      length: 8,
      pool: "qwerty9@",
    });
    registeredUserPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await registration.registration(
      invalidEmail,
      registeredUserPassword,
      registeredUserName
    );

    expect(registrationResponse.statusCode).to.equal(400);
    expect(registrationResponse.body.errors).to.exist;
    expect(registrationResponse.body.errors[0]).to.equal("'Email' is not a valid email address.");
    checkResponseTime(registrationResponse, 1000);
  });

  it("Invalid password registration", async () => {
    registeredUserEmail = chance.email();
    const invalidShortPassword = chance.string({
      length: 2,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await registration.registration(
      registeredUserEmail,
      invalidShortPassword,
      registeredUserName
    );

    expect(registrationResponse.statusCode).to.equal(400);
    expect(registrationResponse.body.errors).to.exist;
    expect(registrationResponse.body.errors[0]).to.equal("Password must be from 4 to 16 characters.");
    checkResponseTime(registrationResponse, 1000);
  });

  it("Invalid password authorization", async () => {
    registeredUserEmail = chance.email();
    registeredUserPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    await registration.registration(
      registeredUserEmail,
      registeredUserPassword,
      registeredUserName
    );

    let invalidCredentialsDataSet = [
      { email: registeredUserEmail, password: "" },
      { email: registeredUserEmail, password: "   " },
      { email: registeredUserEmail, password: "******" },
      { email: registeredUserEmail, password: "Pass 1234" },
      { email: registeredUserEmail, password: "user" },
      { email: registeredUserEmail, password: "1   user" },
      { email: registeredUserEmail, password: registeredUserEmail },
    ];
    invalidCredentialsDataSet.forEach(async (credentials) => {
      loginResponse = await authorization.login(credentials.email, credentials.password);

      expect(loginResponse.statusCode).to.equal(401);
      expect(loginResponse.body.error).to.exist;
      expect(loginResponse.body.error).to.equal(INVALID_PASSWORD_ERROR_MESSAGE);
    });


    it("Invalid user updating", async () => {
      registeredUserId = chance.id();
      registeredUserEmail = chance.email();
      registeredUserName = chance.word({
        length: 4,
        pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
      });
      registeredUserAvatar = chance.word({
        length: 255,
        pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
      });
  
      await registration.registration(
        registeredUserEmail,
        registeredUserPassword,
        registeredUserName
      );
  });
});
});
