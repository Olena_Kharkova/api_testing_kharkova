import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import {
  AuthController,
  RegController,
} from "../lib/controllers/auth.controller";
import {
  checkResponseTime,
  checkStatusCode,
  checkJsonSchema,
} from "../../helpers/functionsForChecking.helper";
import Chance from "chance";
import {
  schema_allUsers,
  schema_login,
  schema_user,
} from "./data/schemas_testData.json";

const users = new UsersController();
const auth = new AuthController();
const reg = new RegController();
const chance = new Chance();

let registeredUSerEmail: string;
let registeredUserPassword: string;
let registeredUserAvatar: string;
let registeredUserName: string;
let registeredUserId: string;
let accessUserToken: string;
let loginResponse: any;
let updatedUserData: any;

describe("Happy Path Tests user", () => {
  before("Register and login", async () => {
    registeredUSerEmail = chance.email();
    registeredUserPassword = chance.string({
      length: 10,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    });
    registeredUserName = chance.word({
      length: 4,
      pool: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    });

    const registrationResponse = await reg.registration(
      registeredUSerEmail,
      registeredUserPassword,
      registeredUserName
    );
    registeredUserId = registrationResponse.body.user.id;
    registeredUserAvatar = registrationResponse.body.user.avatar;

    loginResponse = await auth.login(registeredUSerEmail, registeredUserPassword);
    accessUserToken = loginResponse.body.token.accessToken.token;
  });

  it(`User is registered and logged in`, async () => {
    expect(loginResponse).to.exist;
    let userData = loginResponse.body.user;

    expect(userData.id).to.equal(registeredUserId);
    expect(userData.email).to.equal(registeredUSerEmail);
    expect(userData.userName).to.equal(registeredUserName);

    checkStatusCode(loginResponse, 200);
    checkResponseTime(loginResponse, 1000);
    checkJsonSchema(loginResponse, schema_login);
  });

  it("Get all users", async () => {
    const response = await users.getAllUsers();

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.length).to.be.greaterThan(0);
  });

  it("Get current user details", async () => {
    const response = await users.getCurrentUser(accessUserToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    checkJsonSchema(response.body, schema_user);
    });

  it("Update current user", async () => {
    function replaceLastThreeWithRandom(str: string): string {
      return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
    }

    const updatedUserData = {
      id: registeredUserId,
      avatar: registeredUserAvatar,
      email: registeredUSerEmail,
      userName: replaceLastThreeWithRandom(registeredUserName),
    };
    const response = await users.updateUser(updatedUserData, accessUserToken);

    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
    updatedUserData.userName = updatedUserData.userName;
    checkJsonSchema(updatedUserData, schema_user);
  });

  it("Get current user details again", async () => {
    const response = await users.getCurrentUser(accessUserToken);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.user).to.be.deep.equal(
      updatedUserData,
      "User details isn't correct"
      );
  });

  it("Get user current user by id", async () => {
    const response = await users.getUserById(registeredUserId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
    expect(response.body.user).to.be.deep.equal(
      updatedUserData,
      "User details isn't correct"
    );
  });

  after("Delete current user", async () => {
    const response = await users.deleteUser(accessUserToken, registeredUserId);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
