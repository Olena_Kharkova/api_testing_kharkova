import { ApiRequest } from "../request";

export class AuthController {
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                "email": emailValue,
                "password": passwordValue,
            })
            .send();
        return response;
    }
}
export class RegController {
    async registration(emailValue: string, passwordValue: string, userNameValue: string) {
        const response = await new ApiRequest()
        .prefixUrl("http://tasque.lol/")
        .method("POST")
        .url('api/Register')
        .body({
            "id": 0,
            "avatar": "string",
            "email": emailValue,
            "password": passwordValue,
            "username": userNameValue
        })
        .send();
        return response;
    }
}

