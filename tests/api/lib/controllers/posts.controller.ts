import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async createNewPost(postData : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken (accessToken)
            .send();
        return response;
    }
    async getListOfPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async addLikeForPost(likePostData : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(likePostData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async deleteLikeForPost(likePostData : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(likePostData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    
    async addNewComment(comment : object, accessToken : string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(comment)
            .bearerToken(accessToken)
            .send();
            return response;

    }

}